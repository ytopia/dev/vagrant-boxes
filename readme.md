# vagrant-boxes

## Installation

```bash
curl https://gitlab.com/youtopia.earth/dev/vagrant-boxes/raw/master/install | bash
```

## Usage

install will automatically register to persist all vagrant vm at boot and shutdown

Up all vagrant vm
```bash
vagrant-boxes start
```
Halt all vagrant vm
```bash
vagrant-boxes stop
```
Display status of all vagrant vm
Halt all vagrant vm
```bash
vagrant-boxes status
```

## About
from [https://www.ollegustafsson.com/en/vagrant-suspend-resume/](https://www.ollegustafsson.com/en/vagrant-suspend-resume/)